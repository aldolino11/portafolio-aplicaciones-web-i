var archivojson;
archivojson={"datosestudiantes":[
        {
            "cedula":"1317254625",
            "nombre":"Alex Cedeño",
            "direccion":"El Porvenir",
            "teléfono":"0987654321",
            "correo":"alex@gmail.com",
            "curso":"Quinto",
            "paralelo":"A"
        },
        {
            "cedula":"1314126625",
            "nombre":"Rebeca Castro",
            "direccion":"La Pradera",
            "teléfono":"0987612345",
            "correo":"rebeca@gmail.com",
            "curso":"Quinto",
            "paralelo":"C"
        },
        {
            "cedula":"0956244625",
            "nombre":"Felipe Mantuano",
            "direccion":"Santa Marta",
            "teléfono":"0962312345",
            "correo":"fmantuano@gmail.com",
            "curso":"Quinto",
            "paralelo":"A"
        },
        {
            "cedula":"0910264852",
            "nombre":"Carlos Mero",
            "direccion":"La Paz",
            "teléfono":"0962358647",
            "correo":"carlosm@gmail.com",
            "curso":"Quinto",
            "paralelo":"C"
        },
        {
            "cedula":"1715264328",
            "nombre":"Ismael Román",
            "direccion":"El Pacífico",
            "teléfono":"0946198720",
            "correo":"ismael@gmail.com",
            "curso":"Quinto",
            "paralelo":"B"
        },
        {
            "cedula":"1715248765",
            "nombre":"Luis Imbago",
            "direccion":"Chone",
            "teléfono":"0955548720",
            "correo":"luisimbg@gmail.com",
            "curso":"Quinto",
            "paralelo":"B"
        },
        {
            "cedula":"2413268724",
            "nombre":"Gabriela Fasce",
            "direccion":"Guayaquil",
            "teléfono":"0965123485",
            "correo":"gabyfasce@gmail.com",
            "curso":"Quinto",
            "paralelo":"A"
        },
        {
            "cedula":"0918835752",
            "nombre":"Evelyn Canales",
            "direccion":"Guayaquil",
            "teléfono":"0987516485",
            "correo":"evecan@gmail.com",
            "curso":"Quinto",
            "paralelo":"C"
        },
        {
            "cedula":"0917724684",
            "nombre":"Sergio Soledispa",
            "direccion":"Jipijapa",
            "teléfono":"0998094943",
            "correo":"sergios@gmail.com",
            "curso":"Quinto",
            "paralelo":"C"
        },
        {
            "cedula":"1315248674",
            "nombre":"Anthony Fortty",
            "direccion":"San Pedro",
            "teléfono":"0998776625",
            "correo":"anthony_40@gmail.com",
            "curso":"Quinto",
            "paralelo":"A"
        }
    ]
} 

for(i=0; i<archivojson.datosestudiantes.length; i++){
    document.getElementById('datos').innerHTML = document.getElementById('datos').innerHTML +
    "<p>" + 
    "Cédula: "+ archivojson.datosestudiantes[i].cedula +" " + "</p>" +
    "Nombre: "+archivojson.datosestudiantes[i].nombre+" "+ "</p>" +
    "Dirección: "+archivojson.datosestudiantes[i].direccion+" "+ "</p>" +
    "Teléfono: "+archivojson.datosestudiantes[i].teléfono+" "+ "</p>" +
    "Correo: "+archivojson.datosestudiantes[i].correo+" "+ "</p>" +
    "Curso: "+archivojson.datosestudiantes[i].curso+" "+ "</p>" +
    "Paralelo: "+archivojson.datosestudiantes[i].paralelo + 
    "</p>" +"<br>";
}
