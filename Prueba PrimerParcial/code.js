const formulario = document.getElementById('validacion');
const inputs = document.querySelectorAll('#validacion input');

const expresiones = {
	Código: /^.{5}$/,
    Marca: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, // Letras y espacios, pueden llevar acentos.
    Modelo: /^[a-zA-ZÀ-ÿ\s]{1,30}$/, // Letras y espacios, pueden llevar acentos.
    Ano: /^[0-9]{1,4}$/, //
}
const validarFormulario = (e) => {
	switch (e.target.name) {
		case "codigo":
			if(expresiones.Código.test(e.target.value)){
                document.getElementById("c-error").innerHTML = "";
				document.getElementById("s-error").innerHTML = "";
            } else{
                document.getElementById("c-error").innerHTML = "Código solo puede tener 5 caracteres";
            }
		break;
		case "marca":
			if(expresiones.Marca.test(e.target.value)){
                document.getElementById("t-error").innerHTML = "";
				document.getElementById("s-error").innerHTML = "";
            } else{
                document.getElementById("t-error").innerHTML = "Máximo caracteres permitidos: 50";
            }
		break;
		case "modelo":
			if(expresiones.Modelo.test(e.target.value)){
                document.getElementById("a-error").innerHTML = "";
				document.getElementById("s-error").innerHTML = "";
            } else{
                document.getElementById("a-error").innerHTML = "Máximo caracteres permitidos: 30";
            }
		break;
        case "ano":
			if(expresiones.Ano.test(e.target.value)){
                document.getElementById("e-error").innerHTML = "";
				document.getElementById("s-error").innerHTML = "";
            } else{
                document.getElementById("e-error").innerHTML = "Año solo puede tener 4 dígitos y es tipo numérico";
            }
		break;
        case "fechaini":
            fechaini = document.getElementById("fechaini").value;
            if(fechaini == ""){
                document.getElementById("fp-error").innerHTML = "Debe escoger una fecha para continuar";
            }
            else{
                document.getElementById("fp-error").innerHTML = "";
            }
        break;
        case "fechafin":
            fechaini = document.getElementById("fechaini").value;
            fechafin = document.getElementById("fechafin").value;
            if(fechafin == ""){
                document.getElementById("fp2-error").innerHTML = "Debe escoger una fecha para continuar";
            }
            else{
                document.getElementById("fp2-error").innerHTML = "";
            }
            if(fechafin < fechaini){
                document.getElementById("fi-error").innerHTML = "La fecha de publicación no debe ser mayor a la de ingreso";
            }
            else{
                document.getElementById("fi-error").innerHTML = "";
            }
	}
}
inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    var codigo = document.getElementById('codigo').value;
    var marca = document.getElementById('marca').value;
	var modelo = document.getElementById('modelo').value; 
    var ano = document.getElementById('ano').value; 
    var fechaini = document.getElementById('fechaini').value; 
    var fechafin = document.getElementById('fechafin').value; 
    if(!codigo.trim("") || !marca.trim("") || !modelo.trim("") || !ano.trim("") || 
    !fechaini.trim("") || !fechafin.trim("")){
        document.getElementById("s-error").innerHTML = "Ingrese todos los campos";
    }else{
        document.form1.submit();
        location.reload();
    }
});
