const mysql = require('mysql');
const connection = mysql.createConnection({
host: "localhost",
user: "root",
password: "Server_aldo",
database: "prueba2",
});
connection.connect((error)=>{
if(error) {
console.log('Error de conexión : '+error);
return;
}
console.log('La conexión a la base de datos se la logrado con éxito');
});

//Tener en cuenta el nombre de la base de datos
//node app.js

//1 .- Invocamos a express
const express = require('express');
const app = express();

//2 .- Seteamos urlencoded para capturar los datos del formulario
app.use(express.urlencoded({extended:false}));
app.use(express.json());

//3 .- Establecemos el motor de plantillas ejs
app.set('view engine', 'ejs');

//4 .- Invocamos req y res de express
const req = require('express/lib/request');
const res = require('express/lib/response');
const { redirect } = require('express/lib/response');

app.listen(3000, (req, res) => {
    console.log("SERVER RUNNING IN http://localhost:3000");
});

//5 .- Estableciendo las rutas
app.get("/", (req, res) => {
    res.render("ingresodatos");
});

app.post("/", async (req, res) => {

    //Expresiones regulares
    const expresiones = {
    cedula: /^\d{10}$/, 
    nombres: /^[a-zA-ZÀ-ÿ\s]{1,80}$/, 
    direccion: /^([a-zA-Z0-9_\s]){1,80}$/, 
    telefono: /^\d{10}$/, 
    correo: /\S+@\S+.\S+/, 
    };

    
    datclientes = req.body;
    connection.query("INSERT INTO clientes SET ?", {
      cedula_cliente: datclientes.cedula_cliente,
      nombre_cliente: datclientes.nombre_cliente,
      direccion_cliente: datclientes.direccion_cliente,
      telefono_cliente: datclientes.telefono_cliente,
      email_cliente: datclientes.email_cliente,
    });
    res.render("ingresodatos");
});
